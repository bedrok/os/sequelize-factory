'use strict';

// Dependencies
const log = require('consola');
// const caller = require('caller');
const path = require('path');
const Sequelize = require('sequelize');

// Functions
const delay = (ms) => new Promise((resolve) => setTimeout(resolve, ms));

/**
 * Factory is the primary entry point for the library, and is
 * represented as a Singleton, such that it may only every
 * be instantiated one time. Once instantiated, any attempt to
 * do so a gain, will result in the return the initial
 * instantiation.
 *
 * The Factory itself is plays a supporting role to the already
 * feature rich ORM framework for nodejs - Sequelize. Supporting
 * in that there are general activities you will need to perform
 * outside of the definition and use of the models. This includes
 * how to load the definitions, establishing the associations after
 * all models have been defined, followed by registering hooks and
 * adding other relevant behaviors specific to your implementation.
 */
class Factory {
  // Member variables
  // config = {}
  // models = {}
  // sequelize = null;
  // connection = null;
  // callerModulePath = '';
  // modelPath;

  /**
   * Lorem ipsum dolar sit
   * @return {Factory} an instance of the factory
   */
  constructor() {
    if (!Factory._instance) {
      Factory._instance = this;
      this.callerModulePath = path.dirname(require.main.filename);
    }
    return Factory._instance;
  } // constructor

  /**
   * Returns the Singleton instance of Factory
   * @return {Factory} an instance of the factory
   */
  static getInstance() {
    return this._instance;
  } // static getInstance

  /**
   * Initializes the models and all related items that
   * define their use.
   * @param {Object} options configuration options for the factory
   */
  async initialize(options={}) {
    this.config = options;

    // establish db connection if it does not already exist
    if (!this.sequelize) {
      this.sequelize = new Sequelize(
          options.sequelize.database,
          options.sequelize.username,
          options.sequelize.password,
          options.sequelize,
      );
    }
    await this.loadModels();
    await this.establishAssociations();
    await this.registerHooks();
    await this.applyPlugins();
    await this.executeMigrations();
  } // async initialize

  /**
   * Method is responsible for loading all of the configured
   * models into the factory.
   */
  async loadModels() {
    log.info('Models loading...');
    const {loader} =require('./model');

    // get the modelPath
    this.modelPath = this.config.modelPath ||
      path.join(this.callerModulePath, 'models');
    this.models = await loader({
      sequelize: this.sequelize,
      modelPath: this.modelPath,
    });
    log.success(`Models loaded (${Object.keys(this.models).length})`);
  } // async loadModels

  /**
   *
   */
  async establishAssociations() {
    log.info('Associations establishing...');
    const keys = Object.keys(this.models) || [];
    for (let idx=0; idx < keys.length; idx++) {
      const modelName = keys[idx];
      if (this.models[modelName].defineAssociations) {
        this.models[modelName].defineAssociations(this.models);
      }
    }
    log.success('Associations established');
  } // async establishAssociations

  /**
   *
   */
  async registerHooks() {
    log.info('Hooks registering...');
    await delay(400);
    log.success('Hooks registered');
  } // async registerHooks

  /**
   *
   */
  async applyPlugins() {
    log.info('Plugins applying...');
    const plugins = this.config.plugins || [];
    for (let idx=0; idx < plugins.length; idx++) {
      const plugin = plugins[idx];
      plugin(this.config);
    }
    log.success('Plugins applied');
  } // async applyPlugins

  /**
   *
   */
  async executeMigrations() {
    log.info('Migrations executing...');
    await delay(500);
    log.success('Migrations executed');
  } // async executeMigrations

  /**
   * Gets the list of configured models
   * @return {Object} returns the list of models
   */
  getModels() {
    log.info('getting models');
    return this.models;
  }

  /**
   * Helper returns the path of where the models are located.
   * If not provided in the factory configuration, then it is
   * implied to be located in the calling modules /models
   * folder.
   */
  getModelPath() {

  } // getModelPath
}

module.exports = Factory;
module.exports.Factory = Factory;
module.exports.default = Factory;
