'use strict';

/**
 * file: lib/model.js
 */

// Dependencies
const fs = require('fs');
const path = require('path');
const Sequelize = require('sequelize');

/**
 * Describes the loader
 * @param {Object} config the Sequelize configuration of the connection
 *                        and behavior for the Factory
 *
 * {
 *    sequelize: connection,
 *    modelPath: 'path/to/models'
 * }
 */
module.exports.loader = async function(config) {
  const sequelize = config.sequelize;
  const models = {};
  const modelPath = config.modelPath;

  fs.readdirSync(modelPath)
      .filter((file) => {
        return file !== 'index.js';
      })
      .forEach(async function(file) {
        const definition = require(path.join(modelPath, file));
        const model = await definition(sequelize, Sequelize.DataTypes);
        models[model.name] = model;
      });

  return models;
};
