'use strict';
/**
 * file: lib/plugin/graphql.js
 */
const log = require('consola');

const plugin = async (config) => {
  log.info('Initializing GraphQL Plugin');
  log.success('Initialized GraphQL Plugin');
};

module.exports = plugin;
