'use strict';
/**
 * file: lib/plugin/elasticsearch.js
 */
const log = require('consola');

const plugin = async (config) => {
  log.info('Initializing Elasticsearch Plugin');
  log.success('Initialized Elasticsearch Plugin');
};

module.exports = plugin;
