![Supporter](https://img.shields.io/badge/sponsored%20by-MASHBOT-blue)
<br>
[![MIT](https://img.shields.io/badge/license-MIT-blue)]()
[![Commitizen friendly](https://img.shields.io/badge/commitizen-friendly-brightgreen.svg)](http://commitizen.github.io/cz-cli/)
[![maintained](https://img.shields.io/badge/Maintained%3F-yes-green.svg)]()
[![semantic-release](https://img.shields.io/badge/%20%20%F0%9F%93%A6%F0%9F%9A%80-semantic--release-e10079.svg)](https://github.com/semantic-release/semantic-release)
<br>

# Welcome to Sequelize Factory

For those who have already made the choice to adopt [Sequelize](https://github.com/sequelize/sequelize)
as the ORM for your NodeJS service, you will quickly realize that getting started may not be so out-of-the-box as you would like.

Defining models, associations and hooks etc. are clearly defined and well documented. Loading those models, establishing the associations (in particular) are not.

For example, you must define all models first before attempting to establish the associations. Associating your model to another that has not yet been previously been loaded will lead to an error.
There are many good posts on how to do this, and in fact, it's not that hard. The challenge however is that you must solve this yourself. 

If you are building out a microservice pattern with NodeJS and Sequelize, being consistent is key. Hence, Sequelize Factory was introduced.

You will still define your models the same, including associations and hooks etc., but you will use the Factory to initialize them. In addition to associations and hooks however, the Factory supports the ability of plugins, a feature to allow developers to establish other key characteristics and behaviors of a model in a uniform and consistent way. Examples include configuring a model to support Elasticsearch, or auto-generating GraphQL or gRPC endpoints.

One other key piece to the puzzle is executing the migrations. The factory will also take care of this on initialization after all of the definition and plugin capabilities have been applied. This allows your service to start-up in a consistent manner, apply features in a predictable order, followed by executing the migrations.

Now, as a developer, you need only be focused on the amazing features Sequelize has to offer and leave the initialization and assembly of them to the Factory.

Sequelize Factory follows [Semantic Versioning](https://semver.org/) and supports Node v16 and above.

## Getting started

Installation via NPM
```
$ npm i @bedrok/sequelize-factory --save
```

or via YARN
```
$ yarn add @bedrok/sequelize-factory
```

## Defining your models

As stated above, Sequelize Factory is wrapper to Sequelize that provides additional functionality while simplifying the overall initialization and activation process.

That said, it is opinionated on how you structure your definitions. Specifically, models must use the define method approach and not class extension approach.

Use this method for defining your model;
```javascript
// MyModel.js
module.exports = (sequelize, DataTypes) => {

  const MyModel = sequelize.define(
    'MyModel', {
      id: DataTypes.STRING(32),
      attrA: DataTypes.STRING(20),
      attrB: DataTypes.STRING(20)
    }, {
      tableName: 'my_models'
    }
  );

  return MyModel;
};
```
