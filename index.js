'use strict';

/**
  * The entry point.
  *
  * @module Sequelize
  */
module.exports = require('./lib/factory');
module.exports.Factory = require('./lib/factory');

module.exports.graphQLPlugin = require('./lib/plugin/graphql');
module.exports.elasticsearchPlugin = require('./lib/plugin/elasticsearch');
